FROM golang:1.18-alpine3.16 as builder

RUN apk update && \
    apk add --no-cache upx

WORKDIR /usr/src/app
COPY go.mod go.sum ./
RUN go mod download && go mod verify

COPY . .

RUN CGO_ENABLED=0 go build -ldflags="-w -s" -o /bot ./... && \
    upx /bot


FROM alpine:3.16
LABEL maintainer="DerEnderKeks"

COPY --from=builder /bot /bot
USER 1000:1000
ENV TOKEN ""

ENTRYPOINT ["/bot"]
