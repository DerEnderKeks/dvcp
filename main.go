package main

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"log"
	"os"
	"os/signal"
	"strconv"
	"syscall"
)

func init() {
	log.SetFlags(log.LstdFlags | log.LUTC | log.Lshortfile)
}

func main() {
	token := os.Getenv("TOKEN")
	if len(token) == 0 {
		log.Fatalln("TOKEN is not set")
	}
	startBot(token)
}

type CleanupJob struct {
	GuildID       string
	ChannelID     string
	LastMessageID string
}

type Bot struct {
	session      *discordgo.Session
	cleanupQueue chan CleanupJob
}

func startBot(token string) {
	discord, err := discordgo.New("Bot " + token)
	if err != nil {
		log.Fatalf("failed to create Discord client: %v", err)
	}

	application, err := discord.Application("@me")
	if err != nil {
		log.Fatalln(fmt.Sprintf("failed to get application: %v", err))
	}
	log.Printf("Invite URL: https://discord.com/api/oauth2/authorize?client_id=%s&permissions=74752&scope=bot", application.ID)

	discord.Identify.Intents = discordgo.IntentsGuilds | discordgo.IntentsGuildVoiceStates

	bot := &Bot{
		session:      discord,
		cleanupQueue: make(chan CleanupJob, 100),
	}
	bot.setupHandlers()

	log.Println("connecting to Discord")
	err = discord.Open()
	if err != nil {
		log.Fatalf("failed to open Discord session: %v", err)
	}

	go bot.cleanupJobProcessor()

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	log.Println("shutting down")
	_ = discord.Close()
}

func (b *Bot) setupHandlers() {
	handlers := []any{
		b.readyHandler,
		b.voiceStateHandler,
	}
	for _, h := range handlers {
		b.session.AddHandler(h)
	}
}

func (b *Bot) readyHandler(s *discordgo.Session, _ *discordgo.Ready) {
	err := s.UpdateStatusComplex(discordgo.UpdateStatusData{
		Activities: []*discordgo.Activity{{
			Name: "🔊\U0001F9F9",
			Type: discordgo.ActivityTypeGame,
		}},
		Status: "online",
	})
	if err != nil {
		log.Printf("failed to update status: %v", err)
	}
	log.Printf("connected to Discord as %s", s.State.User.Username)
}

func (b *Bot) voiceStateHandler(s *discordgo.Session, event *discordgo.VoiceStateUpdate) {
	if event.BeforeUpdate == nil || event.BeforeUpdate != nil && event.ChannelID == event.BeforeUpdate.ChannelID {
		return // ignore when not switching channels
	}

	channelUsers, err := b.voiceChannelUsers(event.GuildID, event.BeforeUpdate.ChannelID)
	if err != nil {
		log.Printf("failed to get users in channel: %s: %s: %v", event.GuildID, event.BeforeUpdate.ChannelID, err)
		return
	}

	if len(channelUsers) != 0 {
		return // channel not empty
	}

	channel, err := s.Channel(event.BeforeUpdate.ChannelID)
	if err != nil {
		log.Printf("failed to get channel: %s: %s: %v", event.GuildID, event.BeforeUpdate.ChannelID, err)
		return
	}

	b.cleanupQueue <- CleanupJob{
		GuildID:       event.GuildID,
		ChannelID:     channel.ID,
		LastMessageID: channel.LastMessageID,
	}
}

func (b *Bot) cleanupJobProcessor() {
	for {
		job := <-b.cleanupQueue
		log.Printf("cleaning up channel: %s: %s", job.GuildID, job.ChannelID)
		err := b.purgeVoiceTextMessages(job.GuildID, job.ChannelID, job.LastMessageID)
		if err != nil {
			log.Printf("failed to clean up channel %s: %v", job.ChannelID, err)
		}
	}
}

func (b *Bot) purgeVoiceTextMessages(guildID, channelID, lastMessageID string) error {
	if lastMessageID == "" {
		return nil // no messages in channel
	}

	lastMessageIDInt, err := strconv.Atoi(lastMessageID)
	if err != nil {
		return fmt.Errorf("failed to convert lastMessageID: %s: %v", lastMessageID, err)
	}
	batchMessageID := strconv.Itoa(lastMessageIDInt + 1) // increase by 1 to ensure that the message itself is included

	count := 0

	for {
		messages, err := b.session.ChannelMessages(channelID, 100, batchMessageID, "", "")
		if err != nil {
			return fmt.Errorf("failed to get messages: %s: %s: %v", guildID, channelID, err)
		}

		if len(messages) == 0 {
			break // all done, no messages left
		}

		count += len(messages)
		batchMessageID = messages[len(messages)-1].ID

		messageIDs := make([]string, len(messages))
		for i, message := range messages {
			messageIDs[i] = message.ID
		}

		err = b.session.ChannelMessagesBulkDelete(channelID, messageIDs)
		if err != nil {
			return fmt.Errorf("failed to delete messages: %s: %s: %v", guildID, channelID, err)
		}
	}
	log.Printf("cleaned up %d messages in channel: %s: %s", count, guildID, channelID)
	return nil
}

// voiceChannelUsers returns a list of user IDs in the voice channel
func (b *Bot) voiceChannelUsers(guildID, channelID string) ([]string, error) {
	guild, err := b.session.State.Guild(guildID)
	if err != nil {
		return nil, fmt.Errorf("failed to get guild: %s: %v", guildID, err)
	}

	var users []string
	for _, voiceState := range guild.VoiceStates {
		if voiceState.ChannelID == channelID {
			users = append(users, voiceState.UserID)
		}
	}
	return users, nil
}
