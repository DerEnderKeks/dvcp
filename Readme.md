# Discord Voice Channel Purger

This is a simple Discord bot that purges text messages in voice channels that are empty (as soon as the last person leaves the channel).

The invite URL is printed during startup.

## Installation

#### Docker

```shell
docker run -e TOKEN=<your bot token> registry.gitlab.com/derenderkeks/dvcp:latest
```

#### Bare

Requires Go to be [installed](https://go.dev/doc/install).

```shell
go install gitlab.com/DerEnderKeks/dvcp@main
TOKEN=<your bot token> dvcp
```

### Configuration

The following environment variables are supported:

| Variable | Default | Description                  |
|----------|---------|------------------------------|
| `TOKEN`  |         | Discord bot token (required) |

## License

See [License](License).